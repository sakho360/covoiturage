# Modèle relationnel

City(**id**, name, postal_code, lat, lng, importance)

PostalCode(**postal_code, #city : City.id**)

Address(**id**, number, name, lat, lng, importance, #be_in : City.id)

Vehicle(**number_plate**, brand, model, color)

User(**email**, password, role, first_name, last_name, birthdate, tel_indic, tel_number, activation_date, #reside_at : Address.id)

Trip(**id**, price, seats, valid, #drive : User.email, #use : Vehicle.number_plate)

Stop(**id**, date, time, #trip : Trip.id, #be_at : Address.id)

TripReview(**#from : User.email, #about : User.email, #trip : Trip.id**, rating, comment)

CityLink(**#from_city : City.id, #to_city : City.id**, max_price)

VehicleOwn(**#vehicle : Vehicle.number_plate, #user : User.email**)

StopDeposit(**#user : User.email, #stop : Stop.id**)

StopPickup(**#user : User.email, #stop : Stop.id**)
