\documentclass{article}
\usepackage{multicol}
\setlength{\columnsep}{1cm}

\title{%
    Rapport de projet base de données\\[.25em]%
    \Large{Application de covoiturage}\\[.5em]%
    \normalsize{HLIN511 -- Systèmes d'information et bases de données 2}\\%
    Groupe A7%
}
\author{Rémi Cérès (n°~21509848) \and Mattéo Delabre (n°~21512580)}
\date{\today}

% Police
\usepackage{lmodern}
\usepackage{fontspec}
\usepackage{xunicode}

% Marges
\usepackage{geometry}
\usepackage{multicol}
\geometry{
    a4paper,
    textwidth=17cm,
    textheight=24cm
}

% Francisations
\usepackage{polyglossia}
\setdefaultlanguage{french}
\usepackage[unicode=true]{hyperref}

% Figures
\usepackage{graphicx, grffile}

% Tables
\usepackage{array}
\usepackage{caption}
\usepackage{booktabs}
\usepackage{multirow}

\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}p{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}
\newcommand\spacedots{\hspace{.5em}\dotfill & \dotfill\hspace{.5em}}

% Inclusion PDF
\usepackage{pdfpages}
\usepackage{listings}

% Cadres
\usepackage{framed}

% En-têtes
\usepackage{lastpage}
\usepackage{fancyhdr}

\setlength{\parskip}{.5em plus .25em minus .25em}

\begin{document}

\maketitle

Dans le cadre de l’unité d’enseignement \emph{Systèmes d’information et bases de données 2} (HLIN511), et conjointement avec le module \emph{Architecture et programmation du web} (HLIN510), nous avons été amenés à développer une application web de covoiturage. L’objectif de cette application est de permettre à des utilisateurs enregistrés de participer à ou de proposer des trajets en covoiturage avec leur(s) véhicule(s).

\begin{framed}
\textbf{Fichiers joints}

Les fichiers suivants sont joints à ce rapport :

\begin{itemize}
\item \texttt{functions.sql} -- Définit les fonctions.
\item \texttt{structure.sql} -- Définit les tables et vues.
\item \texttt{triggers.sql} -- Définit les déclencheurs et procédures.
\item \texttt{data.sql} -- Insère les données de test dans la table (230 Mo).
\item \texttt{user.sql} -- Crée l’utilisateur pour accéder à la base avec les permissions strictement nécessaires.
\end{itemize}

Pour fonctionner correctement, ils doivent être importés dans MySQL \emph{exactement dans cet ordre.} L’insertion prend en moyenne quelques minutes.

\textbf{Dépôt Git}

En cas de problème, les fichiers sont disponibles sur le dépôt Git du projet dans le répertoire \texttt{database/}~:

\url{https://gitlab.com/remiceres/covoiturage}
\end{framed}

\section{Choix de conception}

Les choix de conception de l’application sont synthétisés dans le modèle conceptuel, le dictionnaire de données et le modèle relationnel, présentés en annexes A, B et C.

\subsection{Utilisateurs}

L’utilisateur est identifié de manière unique par son adresse email, que nous avons choisie comme clé primaire de la relation \emph{User} qui regroupe tous les attributs caractéristiques des utilisateurs~:

\begin{description}
\item[password] Mot de passe de l’utilisateur, qui doit être saisi pour accéder au compte dans l’interface web. Pour des raisons de sécurité, le mot de passe est hashé avec l’algorithme Bcrypt avant d’être stocké dans la base. Ainsi, il n’est pas possible de retrouver le mot de passe sans le connaître à l’avance.
\item[role] Modélise les différents types de profils tels que décrits dans le sujet. Il est possible pour un utilisateur d’être administrateur ou de ne pas l’être. Le profil « invité » correspond à un utilisateur sans compte utilisateur.
\item[last\_name, first\_name] Nom de famille et prénom de l’utilisateur.
\item[birthdate] Date de naissance de l’utilsateur.
\item[tel\_indic, tel\_number] Numéro de téléphone, décomposé en son indicatif international (33 pour la France) et son numéro réel (usuellement à 9 chiffres).
\item[activation\_date] Contient une date qui est celle à partir de laquelle un utilisateur est autorisé à se connecter sur le site. Cet attribut permet de gérer les fermetures temporaires ou définitives de compte utilisateur. Une fermeture définitive peut être faite en attribuant une date très éloignée.
\end{description}

Les véhicules de l’utilisateur sont référencés dans la relation \emph{Vehicle}. Un utilisateur peut disposer d’un nombre quelconque de voitures, et une voiture appartient à au moins un propriétaire. Les attributs stockés sont~:

\begin{description}
\item[number\_plate] Plaque d’immatriculation du véhicle, utilisée comme clé primaire. Cette clé primaire permet de gérer le cas où deux utilisateurs enregistrent le même véhicule qu’ils souhaitent tous les deux utiliser~: dans ce cas, un seul tuple est stocké dans \emph{Vehicle} et aucune duplication de donnée n’a lieu.
\item[brand, model, color] Fabricant, modèle et couleur du véhicule.
\end{description}

\subsection{Adresses et villes}

La relation \emph{City} stocke chaque ville référencée dans l’application. Elle est liée à un ou plusieurs codes postaux, qui peuvent eux-mêmes appartenir à plusieurs villes. Cette association est réalisée dans \emph{PostalCode}. Enfin, les adresses de la ville sont enregistrées dans la relation \emph{Address} liée à une unique ville.

Pour tester notre application, nous avons décidé d’importer de la Base d’Adresses Nationale Ouverte (BANO\footnote{\url{http://openstreetmap.fr/bano}}) les données des 130 villes les plus importantes de France, et leur 2,4~millions d’adresses.

La relation \emph{City} contient les attributs suivants :

\begin{description}
\item[id] Identifiant unique de la ville, généré séquentiellement.
\item[name] Nom de la ville.
\item[lat, lng] Position géographique de la ville, permettant les recherches par proximité géographique.
\item[importance] Facteur d’importance de la ville, permettant dans une recherche de classer les villes avec la plus grande importance en premier, et ainsi d’améliorer la pertinence des résultats.
\end{description}

La relation \emph{PostalCode} contient les codes postaux de chaque ville, associés à l’identifiant de la ville. Les caractéristiques suivantes des adresses sont enregistrées dans \emph{Address} :

\begin{description}
\item[id] Identifiant unique de l’adresse, généré séquentiellement.
\item[number] Numéro de voie de l’adresse, incluant éventuellement l’extension « bis », « ter », « quater », ...
\item[lat, lng, importance] Même sémantique que pour \emph{City}.
\end{description}

Enfin, les trajets-types fixant des plafonds de prix sur les trajets entre deux villes sont enregistrés sous forme d’une association réflexive sur la relation \emph{City}, avec une ville de départ et une ville d’arrivée.

\subsection{Trajets}

Nous avons choisi de gérer des trajets avec un nombre arbitraire d’étapes au cours desquelles on peut récupérer ou déposer des passagers. Un passager ne peut être déposé que s’il a été récupéré à une étape antérieure. Dans tous les cas, le nombre de passagers à bord ne peut jamais dépasser le plafond fixé par le conducteur. Chaque trajet est localisé à une adresse précise de rendez-vous.

La relation \emph{Trip} référence les attributs globaux au trajet :

\begin{description}
\item[id] Identifiant unique du trajet, généré séquentiellement.
\item[price] Prix au kilomètre du trajet pour chaque passager.
\item[seats] Nombre de places disponibles dans le véhicule.
\item[valid] Cet attribut est passé à 0 dans le cas où le trajet est annulé. Plus aucun passager ne peut alors s’inscrire sur le trajet et les passagers existants sont avertis de l’annulation.
\end{description}

Une fois un passager descendu du véhicule, il a la possibilité de laisser un avis sur ses covoitureurs ou sur le conducteur. De même, une fois le trajet terminé, le conducteur peut laisser un avis sur ses passagers. Dans tous les cas, aucun voyageur ne peut laisser d’avis sur lui-même. Les avis sont référencés par l’association \emph{review} contenant les attributs suivants :

\begin{description}
\item[rating] Note de 0 à 5 sur la personne cible.
\item[comment] Commentaire détaillé optionnel.
\end{description}

Les étapes du trajet sont liées à celui-ci et référencées dans la relation \emph{Stop}. Les trajets qui ne disposent pas d’au moins deux étapes ne permettent le transport d’aucun passager. La date et l’heure de rendez-vous de l’étape sont stockés dans les attributs \textbf{date} et \textbf{time.}

\section{Implémentation des procédures}

\subsection{Fonctions}

\subsubsection{\texttt{NUMERIC get\_occupied\_seats(NUMERIC stop\_id)}}

Cette fonction prend en paramètre une étape d’un trajet et détermine le nombre de passagers restant dans la voiture une fois tous les passagers de cette étape récupérés et déposés.

Pour ce faire, nous comptons d’abord le nombre de passagers récupérés depuis le début du trajet jusqu’à cette étape, puis le nombre de passagers déposés, et nous faisons la différence entre les deux.

Cette fonction est utilisée dans la vérification de la contrainte obligeant le nombre de passagers à chaque étape à être inférieur au plafond fixé par le conducteur.

\subsubsection{\texttt{NUMERIC get\_available\_seats\_between(NUMERIC start\_id, NUMERIC stop\_id)}}

Cette fonction prend en paramètre un couple d’étapes d’un trajet et détermine le nombre de places disponibles pour un voyage allant de la première étape à la seconde. Il s’agit simplement de la différence entre le nombre de places fixé par le conducteur et le maximum des sièges occupés tel que renvoyé par \texttt{get\_occupied\_seats}.

Elle permet d’afficher à l’utilisateur le nombre de places disponibles et de filtrer les sous-trajets par nombre de places.

\subsubsection{\texttt{NUMERIC get\_distance(POINT start, POINT end)}}

Cette fonction prend en paramètre deux positions géographiques et calcule la distance terrestre en kilomètres entre ces deux points. Elle permet de calculer le prix total d’un trajet pour un passager.

\subsubsection{\texttt{NUMERIC get\_time\_estimate(POINT start, POINT end)}}

Cette fonction prend en paramètre deux positions géographiques et estime la durée en minutes d’un trajet en voiture entre ces deux points, en supposant une vitesse moyenne de 80~km/h.

\subsection{Liste des procédures}

Nous avons mis en place une procédure pour chaque table nécessitant une vérification de domaine de valeur, car MySQL ne supporte pas la vérification par \texttt{CHECK}. Ces procédures sont appelées par un déclencheur à chaque insertion ou mise à jour de tuple.

Ci-après, nous décrivons uniquement les autres déclencheurs et procédures que nous avons implémenté.

\subsubsection{\texttt{Trip\_add\_passenger(VARCHAR email, NUMERIC start, NUMERIC end)}}

Ajoute un passager dans un trajet au départ de l’étape spécifiée et à destination de l’étape spécifiée. Il s’agit de la seule façon d’insérer des tuples dans les tables \emph{StopPickup} et \emph{StopDeposit}, qui s’assure qu’on n’insère pas un tuple dans l’une des deux tables sans insérer dans l’autre.

Cette procédure est constituée d’une transaction qui n’est validée que si les deux tuples (récupération et dépôt du passager) sont insérés avec succès. Elle vérifie que toutes les contraintes liées à l’ajout d’un passager soient satisfaites.

\subsubsection{\texttt{Trip\_price\_check(NUMERIC trip)}}

Vérifie que le prix d’un trajet respecte l’éventuel plafond de prix établi par un trajet-type entre les villes.

\subsection{Liste des déclencheurs \emph{(triggers)}}

\subsubsection{\texttt{Trip\_before\_update}}

Lors de la mise à jour d’un trajet, en sus des vérifications de domaines de valeur, nous contrôlons que le prix ne puisse pas être modifié s’il y a déjà des passagers inscrits, et qu’on ne puisse pas réduire le nombre de places disponibles si elles sont déjà réservées pour des passagers.

\subsubsection{\texttt{Stop\_before\_update}}

Ce déclencheur empêche toute modification sur une étape du trajet, notamment son heure, si des passagers y sont déjà inscrits.

\subsubsection{\texttt{StopDeposit\_after\_delete} et \texttt{StopPickup\_after\_delete}}

Ces déclencheurs suppriment automatiquement le tuple de dépôt/récupération d’un passager lorsque l’autre tuple est supprimé. Ainsi, on s’assure que tout voyageur déposé a été récupéré en amont, et inversement.

\subsection{Exemple de requête}

Voici un exemple de requête permettant de rechercher des trajets entre deux villes, en listant les trajets les plus proches des deux villes choisies en premier

\begin{lstlisting}
  SELECT *
    FROM (
         SELECT Trip.id AS id,
                CEIL(price * get_distance(StartAddress.position,
                    EndAddress.position)) AS total_price,
                get_available_seats_between(Start.id, End.id) AS available_seats,

                Start.id AS start_id,
                Start.meet_time AS start_time,
                StartAddress.full_name AS start_point,

                End.id AS end_id,
                End.meet_time AS end_time,
                EndAddress.full_name AS end_point,

                UserDisplay.full_name AS drive_name,
                UserDisplay.rating AS drive_rating,

                get_distance(StartTarget.position,
                    StartAddress.position) AS start_remoteness,
                get_distance(EndTarget.position,
                    EndAddress.position) AS end_remoteness

           FROM Trip,
                UserDisplay,

                Stop AS Start,
                AddressDisplay AS StartAddress,
                City AS StartCity,
                City AS StartTarget,

                Stop AS End,
                AddressDisplay AS EndAddress,
                City AS EndCity,
                City AS EndTarget

          WHERE Start.trip = Trip.id
                AND StartAddress.id = Start.be_at
                AND StartCity.id = StartAddress.be_in
                AND StartTarget.name = 'Montpellier'

                AND End.trip = Trip.id
                AND EndAddress.id = End.be_at
                AND EndCity.id = EndAddress.be_in
                AND EndTarget.name = 'Paris'

                AND UserDisplay.email = Trip.drive
                AND Start.meet_time < End.meet_time
                AND Trip.valid = TRUE
         ) AS ResultTrip

   WHERE start_remoteness <= 20
         AND end_remoteness <= 20
         AND available_seats >= 1
         AND start_time >= NOW()

ORDER BY start_remoteness + end_remoteness ASC;
\end{lstlisting}

\section{Conclusion et perspectives}

La base conçue stocke toutes les données requises par les spécifications et respecte, à notre connaissance, toutes les contraintes qui y sont spécifiées. Par ailleurs, elle a été suffisante pour accéder à toutes les informations nécessaires lors de la réalisation de la partie web du projet.

Ce projet a constitué pour nous une très bonne expérience dans la réalisation d’une base de données plus complexe que celles abordée en TD ou TP. Il nous a notamment permis de consolider nos connaissances en programmation procédurale des bases de données.

\includepdf[pages={-}, pagecommand=\section{Annexes}\subsection*{Annexe A\qquad Modèle conceptuel}, angle=90]{../../../database/conceptuel-A4}

\includepdf[pages={1}, pagecommand=\subsection*{Annexe B\qquad Dictionnaire de données}]{dictionnaire}
\includepdf[pages={2-}]{dictionnaire}

\subsection*{Annexe C\qquad Modèle relationnel}

Pour décrire le modèle relationnel, nous adoptons les conventions suivantes~:

\begin{itemize}
\item la clé primaire de chaque relation est \textbf{en gras}~;
\item les clés étrangères de chaque relation sont précédées d’un croisillon (\#)~;
\item après chaque clé étrangère, la relation et l’attribut référencé par la clé sont spécifiés après un deux-points.
\end{itemize}

\noindent
\begin{framed}
\noindent
City(\textbf{id}, name, postal\_code, lat, lng, importance)

\noindent
PostalCode(\textbf{postal\_code, \#city~: City.id})

\noindent
Address(\textbf{id}, number, name, lat, lng, importance, \#be\_in~: City.id)

\noindent
Vehicle(\textbf{number\_plate}, brand, model, color)

\noindent
User(\textbf{email}, password, role, first\_name, last\_name, birthdate, tel\_indic, tel\_number, activation\_date, \#reside\_at~: Address.id)

\noindent
Trip(\textbf{id}, price, seats, valid, \#drive~: User.email, \#use~: Vehicle.number\_plate)

\noindent
Stop(\textbf{id}, date, time, \#trip~: Trip.id, \#be\_at~: Address.id)

\noindent
TripReview(\textbf{\#from~: User.email, \#about~: User.email, \#trip~: Trip.id}, rating, comment)

\noindent
CityLink(\textbf{\#from\_city~: City.id, \#to\_city~: City.id}, max\_price)

\noindent
VehicleOwn(\textbf{\#vehicle~: Vehicle.number\_plate, \#user~: User.email})

\noindent
StopDeposit(\textbf{\#user~: User.email, \#stop~: Stop.id})

\noindent
StopPickup(\textbf{\#user~: User.email, \#stop~: Stop.id})
\end{framed}


\end{document}
