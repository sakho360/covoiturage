/**
 * Fichier data.sql
 * Contient les ordres d’insertion dans les relations
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */

CREATE DATABASE IF NOT EXISTS covoiturage
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

USE covoiturage;

source data/City.sql
source data/PostalCode.sql
source data/Address.sql
source data/CityLinkData.sql
source data/User.sql
source data/Vehicle.sql
source data/VehicleOwn.sql
source data/Trip.sql
source data/Stop.sql
source data/StopPickup.sql
source data/StopDeposit.sql
source data/TripReview.sql
