/**
 * Fichier TripReview.sql
 * Relation entre User, User et Stop représantant l'avis d'un utilisateur sur
 * un autre utilisateur membre du même voyage. 
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


INSERT INTO `TripReview` VALUES
('adrian.golden@outlook.com', 'fatima.frederick@gmail.com', '1', '5', NULL),
('angelica.ingram@gmail.com', 'fatima.frederick@gmail.com', '1', '2', 'Conductrice très désagréable'),
('beck.taylor@outlook.com', 'fatima.frederick@gmail.com', '1', '5', 'Voyage agréable, conduite prudente et véhicule confortable'),
('chaney.alexander@umontpellier.fr', 'fatima.frederick@gmail.com', '1', '4', NULL),
('ciaran.wilkinson@yahoo.com', 'april.vance@hotmail.com', '3', '2', 'Passagère en retard de 25 minutes'),
('damian.page@lilo.org', 'ciaran.wilkinson@yahoo.com', '3', '3', 'Conducteur peu cordial mais voyage agréable'),
('fatima.frederick@gmail.com', 'angelica.ingram@gmail.com', '1', '1', NULL),
('francis.ramos@outlook.com', 'ciaran.wilkinson@yahoo.com', '3', '2', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
