/**
 * Fichier Vehicle.sql
 * Contient des données sur les vehicules
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


INSERT INTO `Vehicle` VALUES
('AS848ZQ', 'BMW', 'M5', 'orange'),
('BT877RX', 'Dodge', 'Journey', 'orange'),
('BW192PM', 'Mitsubishi', 'Chariot', 'mauve'),
('CA389WV', 'Oldsmobile', 'Bravada', 'jaune'),
('CY818VR', 'Chevrolet', 'Volt', 'bleu'),
('DA300FO', 'Chrysler', 'Imperial', 'noir'),
('DI271AO', 'Chevrolet', 'Uplander', 'violet'),
('DI300FA', 'Toyota', '4Runner', 'rouge'),
('DI501AK', 'Lexus', 'GS', 'mauve'),
('DL795CE', 'BMW', 'Z3', 'gris'),
('EM473OS', 'Buick', 'Park Avenue', 'orange'),
('FA406ZM', 'Ford', 'Econoline E150', 'gris'),
('FM045CD', 'Mazda', 'Familia', 'bleu'),
('FV978UC', 'Volkswagen', 'Golf III', 'noir'),
('GK511HX', 'Mercedes-Benz', 'C-Class', 'blanc'),
('GW777SH', 'Dodge', 'Ram 3500', 'rouge'),
('HN986LT', 'Dodge', 'Journey', 'jaune'),
('HX771SK', 'Honda', 'Prelude', 'blanc'),
('IX662GF', 'Pontiac', 'Grand Prix Turbo', 'vert'),
('JS852BX', 'Hyundai', 'Scoupe', 'orange'),
('LD562WK', 'Subaru', 'Outback', 'bleu'),
('LU823KL', 'Honda', 'Pilot', 'jaune'),
('MH428OR', 'Nissan', 'Pathfinder', 'jaune'),
('ML756JT', 'Cadillac', 'CTS-V', 'vert'),
('NK082SY', 'Chrysler', 'Town & Country', 'blanc'),
('NZ927WC', 'Audi', 'Allroad', 'mauve'),
('OK427WO', 'GMC', 'Envoy', 'rouge'),
('OQ341RU', 'Lincoln', 'LS', 'blanc'),
('OR113GP', 'Lincoln', 'Navigator L', 'bleu'),
('OY587YJ', 'Scion', 'tC', 'blanc'),
('PX349UZ', 'Oldsmobile', 'Toronado', 'jaune'),
('PY165VW', 'Mitsubishi', 'Pajero', 'gris'),
('QU274FT', 'Honda', 'Odyssey', 'violet'),
('QV053RZ', 'Kia', 'Amanti', 'jaune'),
('RJ381QN', 'Geo', 'Metro', 'bleu'),
('TC815ON', 'Ford', 'Fusion', 'violet'),
('TG998IP', 'Land Rover', 'Range Rover Sport', 'jaune'),
('TO261QQ', 'Oldsmobile', '88', 'violet'),
('TO468TA', 'Mitsubishi', 'i-MiEV', 'violet'),
('VE880DL', 'Chevrolet', 'Tahoe', 'mauve'),
('WM003BI', 'Chevrolet', 'Lumina APV', 'bleu'),
('WR846EJ', 'Kia', 'Optima', 'blanc'),
('WZ583ZY', 'Audi', 'RS4', 'noir'),
('XV310EF', 'Toyota', 'Tacoma', 'jaune'),
('YS748KR', 'Chevrolet', 'Caprice', 'violet'),
('ZC107PL', 'Daewoo', 'Nubira', 'gris');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
