/**
 * Fichier Trip.sql
 * Contient des données sur les voyages
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


INSERT INTO `Trip` VALUES
('1', '10', '4', 1, 'fatima.frederick@gmail.com', 'XV310EF'),
('2', '15', '3', 1, 'carter.wolfe@umontpellier.fr', 'DI501AK'),
('3', '8', '2', 1, 'ciaran.wilkinson@yahoo.com', 'TG998IP'),
('4', '9', '3', 1, 'mary.bradley@yahoo.com', 'DI300FA'),
('5', '12', '5', 1, 'tyler.sparks@outlook.com', 'CY818VR');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
