/**
 * Fichier VehicleOwn.sql
 * Relation entre User et Vehile représantant les voitures appartenant a un 
 * utilisateur
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


INSERT INTO `VehicleOwn` VALUES
('DI501AK', 'adara.byers@hotmail.com'),
('RJ381QN', 'adara.byers@hotmail.com'),
('OY587YJ', 'adrian.golden@outlook.com'),
('DL795CE', 'akeem.foster@outlook.com'),
('QU274FT', 'akeem.foster@outlook.com'),
('FV978UC', 'alden.morse@umontpellier.fr'),
('LD562WK', 'alec.mejia@gmail.com'),
('MH428OR', 'alec.mejia@gmail.com'),
('DA300FO', 'anastasia.kline@yahoo.com'),
('XV310EF', 'angelica.ingram@gmail.com'),
('FM045CD', 'april.vance@hotmail.com'),
('NZ927WC', 'azalia.prince@yahoo.com'),
('JS852BX', 'baxter.simon@hotmail.com'),
('LD562WK', 'beck.taylor@outlook.com'),
('ML756JT', 'benedict.carson@outlook.com'),
('PX349UZ', 'brendan.hill@gmail.com'),
('DI501AK', 'carter.wolfe@umontpellier.fr'),
('EM473OS', 'chaney.alexander@umontpellier.fr'),
('TG998IP', 'ciaran.wilkinson@yahoo.com'),
('TO261QQ', 'ciaran.wilkinson@yahoo.com'),
('AS848ZQ', 'damian.page@lilo.org'),
('WR846EJ', 'dorian.chandler@hotmail.com'),
('FA406ZM', 'elizabeth.stephens@hotmail.com'),
('PY165VW', 'ezekiel.mclaughlin@gmail.com'),
('BW192PM', 'fatima.frederick@gmail.com'),
('XV310EF', 'fatima.frederick@gmail.com'),
('HX771SK', 'fiona.mccullough@umontpellier.fr'),
('ML756JT', 'fiona.mccullough@umontpellier.fr'),
('OK427WO', 'francis.ramos@outlook.com'),
('ZC107PL', 'francis.ramos@outlook.com'),
('TO468TA', 'galena.fernandez@yahoo.com'),
('DI271AO', 'gavin.kerr@yahoo.com'),
('JS852BX', 'gavin.kerr@yahoo.com'),
('IX662GF', 'gemma.burns@lilo.org'),
('DI300FA', 'howard.lambert@outlook.com'),
('YS748KR', 'howard.lambert@outlook.com'),
('NK082SY', 'iliana.nielsen@outlook.com'),
('VE880DL', 'iliana.nielsen@outlook.com'),
('TO468TA', 'isadora.mayer@yahoo.com'),
('OQ341RU', 'jesse.price@gmail.com'),
('WM003BI', 'jesse.price@gmail.com'),
('CY818VR', 'john.blackburn@yahoo.com'),
('QV053RZ', 'joy.sampson@hotmail.com'),
('EM473OS', 'kathleen.ward@outlook.com'),
('PX349UZ', 'kathleen.ward@outlook.com'),
('LU823KL', 'kaye.burke@lilo.org'),
('QV053RZ', 'kaye.burke@lilo.org'),
('GW777SH', 'marshall.luna@yahoo.com'),
('DI300FA', 'mary.bradley@yahoo.com'),
('OQ341RU', 'micah.wagner@yahoo.com'),
('FM045CD', 'patience.schwartz@umontpellier.fr'),
('OY587YJ', 'quinn.turner@umontpellier.fr'),
('TC815ON', 'quinn.turner@umontpellier.fr'),
('GK511HX', 'rinah.wynn@hotmail.com'),
('IX662GF', 'rinah.wynn@hotmail.com'),
('BT877RX', 'tara.higgins@yahoo.com'),
('CY818VR', 'tyler.sparks@outlook.com'),
('HN986LT', 'tyler.sparks@outlook.com'),
('CA389WV', 'ulysses.gibbs@lilo.org'),
('OR113GP', 'ulysses.gibbs@lilo.org'),
('AS848ZQ', 'uriel.banks@umontpellier.fr'),
('WZ583ZY', 'uriel.banks@umontpellier.fr'),
('NK082SY', 'wallace.silva@yahoo.com'),
('OR113GP', 'winter.horne@umontpellier.fr'),
('ZC107PL', 'winter.horne@umontpellier.fr');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
