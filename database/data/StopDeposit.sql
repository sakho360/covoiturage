/**
 * Fichier StopDeposit.sql
 * Relation entre User et Stop représantant le dépot d'un passager à un arrêt
 *
 * Mattéo Delabre (21512580)
 * Rémi Cérès (21509848)
 */
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


INSERT INTO `StopDeposit` VALUES
('adrian.golden@outlook.com', '2'),
('angelica.ingram@gmail.com', '2'),
('beck.taylor@outlook.com', '2'),
('chaney.alexander@umontpellier.fr', '3'),
('april.vance@hotmail.com', '7'),
('damian.page@lilo.org', '8'),
('francis.ramos@outlook.com', '8'),
('jesse.price@gmail.com', '11'),
('tara.higgins@yahoo.com', '12'),
('howard.lambert@outlook.com', '13');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
