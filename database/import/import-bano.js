const fs = require('fs');
const readline = require('readline');

const output = process.stdout;
const input = process.argv.length === 3
    ? fs.createReadStream(process.argv[2])
    : process.stdin;

output.write('START TRANSACTION');

const rl = readline.createInterface({
    input,
    crlfDelay: Infinity
});

const cityIds = new Map();
const postalCodes = new Map();

const writeTuple = (() =>
{
    let currentBatch = '';
    let currentSize = 0;
    let firstRow = true;

    const startBatch = table =>
    {
        if (table === currentBatch)
        {
            return;
        }

        output.write(`; INSERT INTO ${table} VALUES `);
        currentBatch = table;
        currentSize = 0;
        firstRow = true;
    };

    return (table, tuple) =>
    {
        if (currentSize === 2048)
        {
            currentBatch = '';
        }

        startBatch(table);

        if (firstRow)
        {
            firstRow = false;
        }
        else
        {
            output.write(', ');
        }

        currentSize++;
        output.write(tuple);
    };
})();

let nextCityId = 1;
let nextAddressId = 1;

rl.on('line', line =>
{
    const data = JSON.parse(line);

    // Import des villes avec déduplication des codes postaux
    if (data.type === 'municipality')
    {
        const nameKey = data.name.replace(/[- ]/g, '').toLowerCase();
        const name = data.name.replace(/'/g, '\'\'');
        const lat = data.lat;
        const lng = data.lon;
        const importance = data.importance;

        if (importance < 0.55)
        {
            return;
        }

        if (!cityIds.has(nameKey))
        {
            const id = nextCityId++;
            postalCodes.set(id, [data.postcode]);
            cityIds.set(nameKey, id);

            writeTuple(
                'City',
                `(${id}, '${name}', POINT(${lng}, ${lat}), ${importance})`
            );
        }
        else
        {
            const id = cityIds.get(nameKey);

            if (!postalCodes.get(id).includes(data.postcode))
            {
                postalCodes.get(id).push(data.postcode);
            }
        }
    }

    // Import des adresses avec duplication par numéro de rue
    if (data.type == 'street')
    {
        const cityNameKey = data.city.replace(/[- ]/g, '').toLowerCase();
        const name = data.name[0].replace(/'/g, '\'\'');
        const postcode = data.postcode;
        const importance = data.importance;

        if (!cityIds.has(cityNameKey) || name.length > 256)
        {
            return;
        }

        const cityId = cityIds.get(cityNameKey);

        if (!postalCodes.get(cityId).includes(postcode))
        {
            postalCodes.get(cityId).push(postcode);
        }

        for (let key in data.housenumbers)
        {
            let number = parseInt(key, 10);
            const lat = data.housenumbers[key].lat;
            const lng = data.housenumbers[key].lon;

            if (data.housenumbers.hasOwnProperty(key) && number < 9999)
            {
                const id = nextAddressId++;

                if (number === 0)
                {
                    number = '';
                }

                writeTuple(
                    'Address',
                    `(${id}, '${number}', '${name}', POINT(${lng}, ${lat}), ${importance}, ${cityId})`
                );
            }
        }
    }
});

rl.on('close', line =>
{
    for (let [id, codes] of postalCodes)
    {
        codes.forEach(code =>
        {
            writeTuple('PostalCode', `('${code}', ${id})`);
        });
    }

    output.write('; COMMIT;');
});

