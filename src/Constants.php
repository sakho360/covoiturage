<?php
/**
 * Liste de constantes utiles dans l’application.
 */
class Constants
{
    // Format des types DATETIME dans MySQL
    public const DATETIME_MYSQL_FORMAT = 'Y-m-d H:i:s';

    // Format ISO des dates
    public const DATE_ISO_FORMAT = 'Y-m-d';

    // Format ISO des heures
    public const TIME_ISO_FORMAT = 'H:i:s';

    // Format ISO des heures sans secondes
    public const SHORT_TIME_ISO_FORMAT = 'H:i';

    // Format des dates avec heure à afficher à l’utilisateur
    public const DATETIME_USER_FORMAT = '%a %e %B %Y, %H h %M';
}
