<?php
namespace Trip\Search;

/**
 * Vue pour la page de recherche d’un trajet.
 */
class SearchView implements \IView
{
    private $form;
    private $results;

    /**
     * Construit une nouvelle page de recherche de trajet.
     *
     * @param form Vue de formulaire de recherche.
     * @param results Vue des résultats de la recherche.
     */
    public function __construct(FormView $form, ResultsView $results)
    {
        $this->form = $form;
        $this->results = $results;
    }

    public function render()
    {
?>
<h2>Rechercher un trajet</h2>
<?php
        $this->form->render();

        if (!empty($_GET)):
?>
<h2>Résultats de la recherche</h2>
<?php
            $this->results->render();
        endif;
    }
}

