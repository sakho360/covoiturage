<?php
namespace Trip\Search;

/**
 * Modèle pour les recherches de trajet.
 */
class SearchModel
{
    // Référence à la connexion vers la base de données
    private $_db;

    // Ville de départ souhaitée pour le trajet
    private $_start_city;

    // Ville d’arrivée souhaitée pour le trajet
    private $_end_city;

    // Heure et date de départ souhaitée pour le trajet
    private $_start_time;

    // Heure et date d’arrivée souhaitée pour le trajet
    private $_end_time;

    // Prix maximum à ne pas dépasser pour le trajet
    private $_max_price = PHP_INT_MAX;

    // Note minimale du conducteur/de la conductrice du trajet souhaitée
    private $_min_rating = 0;

    // Nombre de places demandées
    private $_nb_places = 1;

    // Ordre de tri des résultats de la recherche
    private $_order = [Ordering::PROXIMITY, Ordering::PRICE];

    /**
     * Construit le modèle de Trip/Search
     *
     * @param db Référence à la connexion vers la base de données à utiliser
     * pour récupérer les données de ce modèle.
     */
    public function __construct(\PDO $db)
    {
        $this->_db = $db;
    }

    /**
     * Récupère la liste des villes dans la base.
     *
     * @return array Liste de noms de villes.
     */
    public function fetchCities()
    {
        $stmt = $this->_db->query(<<<SQL
SELECT City.name
  FROM City
SQL
        );

        return $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
    }

    /**
     * Récupère la liste des trajets correspondants.
     *
     * @return array(stdClass) Liste des trajets correspondants
     */
    public function fetchResults()
    {
        // Si la ville de départ ou d’arrivée ne sont pas
        // renseignés, on ne renvoie rien
        if ($this->_start_city === NULL || $this->_end_city === NULL)
        {
            return [];
        }

        // Construction des parties variables de la requête
        $query_vars = [];
        $condition_start_time = '';

        if ($this->_start_time !== NULL)
        {
            $condition_start_time = 'AND start_time >= :start_time';
            $query_vars[':start_time'] = $this->_start_time->format(
                \Constants::DATETIME_MYSQL_FORMAT
            );
        }
        else
        {
            $condition_start_time = 'AND start_time >= NOW()';
        }

        $condition_end_time = '';

        if ($this->_end_time !== NULL)
        {
            $condition_end_time = 'AND end_time <= :end_time';
            $query_vars[':end_time'] = $this->_end_time->format(
                \Constants::DATETIME_MYSQL_FORMAT
            );
        }

        $ordering = implode(', ', array_map(
            function ($order)
            {
                switch ($order)
                {
                    case Ordering::PROXIMITY:
                        return 'start_remoteness + end_remoteness ASC';

                    case Ordering::DATE:
                        return 'start_time ASC, end_time ASC';

                    case Ordering::PRICE:
                        return 'total_price ASC';

                    case Ordering::RATING:
                        return 'drive_rating DESC';

                    default:
                        throw new \InvalidArgumentException(
                            'Ordre de tri de la recherche invalide : '
                            . $order
                        );
                }
            },
            $this->_order
        ));

        // Construction de la requête principale
        $query = <<<SQL
  SELECT *
    FROM (
         SELECT Trip.id AS id,
                CEIL(price * get_distance(StartAddress.position, EndAddress.position)) AS total_price,
                get_available_seats_between(Start.id, End.id) AS available_seats,

                Start.id AS start_id,
                Start.meet_time AS start_time,
                StartAddress.full_name AS start_point,

                End.id AS end_id,
                End.meet_time AS end_time,
                EndAddress.full_name AS end_point,

                UserDisplay.full_name AS drive_name,
                UserDisplay.rating AS drive_rating,

                get_distance(StartTarget.position, StartAddress.position) AS start_remoteness,
                get_distance(EndTarget.position, EndAddress.position) AS end_remoteness

           FROM Trip,
                UserDisplay,

                Stop AS Start,
                AddressDisplay AS StartAddress,
                City AS StartCity,
                City AS StartTarget,

                Stop AS End,
                AddressDisplay AS EndAddress,
                City AS EndCity,
                City AS EndTarget

          WHERE Start.trip = Trip.id
                AND StartAddress.id = Start.be_at
                AND StartCity.id = StartAddress.be_in
                AND StartTarget.name = :start_city

                AND End.trip = Trip.id
                AND EndAddress.id = End.be_at
                AND EndCity.id = EndAddress.be_in
                AND EndTarget.name = :end_city

                AND UserDisplay.email = Trip.drive
                AND Start.meet_time < End.meet_time
                AND Trip.valid = TRUE
         ) AS ResultTrip

   WHERE start_remoteness <= 20
         AND end_remoteness <= 20
         AND total_price <= :max_price
         AND (
             drive_rating >= :min_rating
             OR drive_rating IS NULL
         )
         AND available_seats >= :nb_places
         {$condition_start_time}
         {$condition_end_time}

ORDER BY {$ordering};
SQL;

        $query_vars += array(
            ':max_price' => $this->_max_price,
            ':min_rating' => $this->_min_rating,
            ':nb_places' => $this->_nb_places,
            ':start_city' => $this->_start_city,
            ':end_city' => $this->_end_city
        );

        // Préparation et exécution de la requête
        $stmp = $this->_db->prepare($query);
        $stmp->execute($query_vars);

        $results = $stmp->fetchAll(\PDO::FETCH_OBJ);

        // Transformation des dates en instances de DateTime
        foreach ($results as $result)
        {
            $result->start_time = \DateTime::createFromFormat(
                \Constants::DATETIME_MYSQL_FORMAT,
                $result->start_time
            );

            $result->end_time = \DateTime::createFromFormat(
                \Constants::DATETIME_MYSQL_FORMAT,
                $result->end_time
            );
        }

        return $results;
    }

    /**
     * Récupère la ville de départ recherchée.
     *
     * @return Ville de départ recherchée, ou NULL si aucune ville
     * n’a encore été spécifiée.
     */
    public function getStartCity()
    {
        return $this->_start_city;
    }

    /**
     * Définit la ville de départ recherchée.
     *
     * @param start_city Ville de départ recherchée.
     */
    public function setStartCity($start_city)
    {
        $this->_start_city = $start_city;
    }

    /**
     * Récupère la ville d’arrivée recherchée.
     *
     * @return Ville d’arrivée recherchée, ou NULL si aucune ville
     * n'a encore été spécifiée.
     */
    public function getEndCity()
    {
        return $this->_end_city;
    }

    /**
     * Définit la ville d’arrivée recherchée.
     *
     * @param start_city Ville d’arrivée recherchée.
     */
    public function setEndCity($end_city)
    {
        $this->_end_city = $end_city;
    }

    /**
     * Récupère l’heure et la date de départ de la recherche.
     *
     * @return Heure et date de départ, ou NULL si aucune heure n’a
     * encore été spécifiée.
     */
    public function getStartTime()
    {
        return $this->_start_time;
    }

    /**
     * Définit l’heure et la date de départ souhaitée.
     *
     * @param start_time Heure et date de départ.
     */
    public function setStartTime(\DateTime $start_time)
    {
        $this->_start_time = $start_time;
    }

    /**
     * Récupère l’heure et la date d’arrivée de la recherche.
     *
     * @return Heure et date d’arrivée, ou NULL si aucune heure n’a
     * encore été spécifiée.
     */
    public function getEndTime()
    {
        return $this->_end_time;
    }

    /**
     * Définit l’heure et la date d’arrivée souhaitée.
     *
     * @param end_time Heure et date d’arrivée.
     */
    public function setEndTime(\DateTime $end_time)
    {
        $this->_end_time = $end_time;
    }

    /**
     * Récupère le prix maximum d'un trajet de la recherche.
     *
     * @return Price maximum du trajet
     */
    public function getMaxPrice()
    {
        return $this->_max_price;
    }

    /**
     * Spécifie le prix maximum total recherché pour le trajet.
     *
     * @param max_price Prix maximum en centimes d’euros.
     */
    public function setMaxPrice($max_price)
    {
        $this->_max_price = max(0, intval($max_price));
    }

    /**
     * Récupère la note minimale du conducteur/de la conductrice
     * demandée pour ce trajet.
     *
     * @return Note minimale entre 0 et 5.
     */
    public function getMinRating()
    {
        return $this->_min_rating;
    }

    /**
     * Définit la note minimale du conducteur/de la conductrice pour
     * le trajet recherché.
     *
     * @param min_rating Note minimale entre 0 et 5.
     */
    public function setMinRating($min_rating)
    {
        $this->_min_rating = min(5, max(0, intval($min_rating)));
    }

    /**
     * Récupère le nombre de place disponible minimum requis pour un trajet
     *
     * @return Number de place disponible requis
     */
    public function getNbPlaces()
    {
        return $this->_nb_places;
    }

    /**
     * Définit le nombre de places recherchées pour le trajet.
     *
     * @param nb_places Nombre de places recherchées.
     */
    public function setNbPlaces($nb_places)
    {
        $this->_nb_places = max(0, intval($nb_places));
    }

    /**
     * Récupère l’ordre de tri des résultats pour la recherche.
     *
     * @return Liste des tris choisis.
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * Définit l'ordre de tri des résultats pour la recherche.
     *
     * @param order Ordres de tri du plus important au moins important.
     */
    public function setOrder(array $order)
    {
        foreach ($order as $order_value)
        {
            if (
                $order_value !== Ordering::PROXIMITY
                    && $order_value !== Ordering::DATE
                    && $order_value !== Ordering::PRICE
                    && $order_value !== Ordering::RATING
            )
            {
                throw new \InvalidArgumentException(
                    'Ordre de tri de la recherche invalide : ' . $order_value
                );
            }
        }

        $this->_order = $order;
    }
}
