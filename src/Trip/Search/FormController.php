<?php
namespace Trip\Search;

/**
 * Contrôleur du formulaire de recherche.
 */
class FormController
{
    private $search;
    private $form;

    public function __construct(SearchModel $search, \Form\Model $form)
    {
        $this->search = $search;
        $this->form = $form;
    }

    public function search($data)
    {
        if (!empty($data['start-city']))
        {
            $this->search->setStartCity($data['start-city']);
        }

        if (!empty($data['end-city']))
        {
            $this->search->setEndCity($data['end-city']);
        }

        if (!empty($data['start-date']))
        {
            $date = $data['start-date'];
            $time = '00:00:00';

            if (!empty($data['start-time']))
            {
                $time = $data['start-time'] . ':00';
            }

            $start_time = \DateTime::createFromFormat(
                \Constants::DATETIME_MYSQL_FORMAT,
                $date . ' ' . $time
            );

            if ($start_time instanceof \DateTime)
            {
                try
                {
                    $this->search->setStartTime($start_time);
                }
                catch (\InvalidArgumentException $err)
                {
                    $this->form->addError(
                        'start-time',
                        $err->getMessage()
                    );
                }
            }
            else
            {
                $this->form->addError(
                    'start-time',
                    'Format de date ou d’heure invalide.'
                );
            }
        }

        if (!empty($data['end-date']))
        {
            $date = $data['end-date'];
            $time = '00:00:00';

            if (!empty($data['end-time']))
            {
                $time = $data['end-time'] . ':00';
            }

            $end_time = \DateTime::createFromFormat(
                \Constants::DATETIME_MYSQL_FORMAT,
                $date . ' ' . $time
            );

            if ($end_time instanceof \DateTime)
            {
                try
                {
                    $this->search->setEndTime($end_time);
                }
                catch (\InvalidArgumentException $err)
                {
                    $this->form->addError(
                        'end-time',
                        $err->getMessage()
                    );
                }
            }
            else
            {
                $this->form->addError(
                    'end-time',
                    'Format de date ou d’heure invalide.'
                );
            }
        }

        if (!empty($data['max-price']))
        {
            if (is_numeric($data['max-price']))
            {
                try
                {
                    $this->search->setMaxPrice(
                        floatval($data['max-price']) * 100
                    );
                }
                catch (\InvalidArgumentException $err)
                {
                    $this->form->addError(
                        'max-price',
                        $err->getMessage()
                    );
                }
            }
            else
            {
                $this->form->addError(
                    'max-price',
                    'Le prix maximal doit être au format numérique.'
                );
            }
        }

        if (!empty($data['min-rating']))
        {
            if (is_numeric($data['min-rating']))
            {
                try
                {
                    $this->search->setMinRating(
                        intval($data['min-rating'])
                    );
                }
                catch (\InvalidArgumentException $err)
                {
                    $this->form->addError(
                        'min-rating',
                        $err->getMessage()
                    );
                }
            }
            else
            {
                $this->form->addError(
                    'min-rating',
                    'La note minimale doit être au format numérique.'
                );
            }
        }

        if (!empty($data['nb-places']))
        {
            if (is_numeric($data['nb-places']))
            {
                try
                {
                    $this->search->setNbPlaces(
                        intval($data['nb-places'])
                    );
                }
                catch (\InvalidArgumentException $err)
                {
                    $this->form->addError(
                        'nb-places',
                        $err->getMessage()
                    );
                }
            }
            else
            {
                $this->form->addError(
                    'nb-places',
                    'Le nombre de places doit être au format numérique.'
                );
            }
        }

        if (!empty($data['order']))
        {
            switch ($data['order'])
            {
            case 'proximity':
                $this->search->setOrder([Ordering::PROXIMITY]);
                break;

            case 'date':
                $this->search->setOrder([Ordering::DATE]);
                break;

            case 'price':
                $this->search->setOrder([Ordering::PRICE]);
                break;

            case 'rating':
                $this->search->setOrder([Ordering::RATING]);
                break;

            default:
                $this->form->addError(
                    'order',
                    $data['order'] . ' n’est pas un ordre de tri valide.'
                );
                break;
            }
        }
    }
}

