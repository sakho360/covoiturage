<?php
namespace Trip\Search;

/**
 * Vue pour les résultats de recherche de trajet.
 */
class ResultsView implements \IView
{
    private $router;
    private $model;

    /**
     * Construit une nouvelle vue affichant les résultats d’une recherche
     * de trajet.
     *
     * @param router Instance du routeur.
     * @param model Modèle de recherche de trajet.
     */
    public function __construct(\Router $router, SearchModel $model)
    {
        $this->router = $router;
        $this->model = $model;
    }

    public function render()
    {
        $list = $this->model->fetchResults();
?>
<table class="solid">
    <tr>
        <th>Date de départ</th>
        <th>Lieu de départ</th>
        <th>Date d’arrivée</th>
        <th>Lieu d’arrivée</th>
        <th>Prix</th>
        <th>Conducteur</th>
        <th>Détails</th>
    </tr>
    <?php
    if (empty($list)):
    ?>
    <tr>
        <td colspan="7">Aucun résultat</td>
    </tr>
    <?php
    else: foreach ($list as $result):
    ?>
    <tr>
        <td><?= strftime(\Constants::DATETIME_USER_FORMAT, $result->start_time->getTimestamp()) ?></td>
        <td><?= $result->start_point ?></td>
        <td><?= strftime(\Constants::DATETIME_USER_FORMAT, $result->end_time->getTimestamp()) ?></td>
        <td><?= $result->end_point ?></td>
        <td><?= money_format('%.2n', $result->total_price / 100) ?></td>
        <td>
            <?= $result->drive_name ?><br>
            <?php
            if ($result->drive_rating === NULL):
            ?>
            Non-noté·e
            <?php
            else:
            ?>
            Note : <?= number_format($result->drive_rating, 1, ',', '') ?> / 5
            <?php
            endif;
            ?>
        </td>
        <td>
            <a href="<?= $this->router->generate(
                'tripShow', array(
                    'trip' => $result->id,
                    'start' => $result->start_id,
                    'end' => $result->end_id
                )
            ) ?>">Détails »</a>
        </td>
    </tr>
    <?php
    endforeach;
    endif;
    ?>
</table>
<?php
    }
}
