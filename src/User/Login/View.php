<?php
namespace User\Login;

/**
 * Vue pour le formulaire de connexion.
 */
class View implements \IView
{
    private $model;
    private $form;

    public function __construct(Model $model, \Form\Model $form)
    {
        $this->model = $model;
        $this->form = $form;
    }

    public function render()
    {
?>
<h2>Connexion</h2>

<form method="POST" class="solid">
    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'email');
    ?>
    <p>
        <label for="user-login-form-email">Adresse email :</label>
        <input type="email" id="user-login-form-email" required
            name="email" value="<?= $this->model->getEmail() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'password');
    ?>
    <p>
        <label for="user-login-form-password">Mot de passe :</label>
        <input type="password" id="user-login-form-password" required
            name="password">
    </p>

    <p>
        <input type="submit" value="Se connecter">
    </p>
</form>
<?php
    }
}
