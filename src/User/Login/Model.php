<?php
namespace User\Login;

/**
 * Modèle pour le formulaire de connexion.
 */
class Model
{
    // Email utilisé pour la connexion
    private $email;

    /**
     * Définit l’email utilisé pour la connexion.
     *
     * @param email Email ayant été utilisé.
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Récupère l’email utilisé pour la connexion.
     *
     * @return Email ayant été utilisé.
     */
    public function getEmail()
    {
        return $this->email;
    }
}
