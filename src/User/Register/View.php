<?php
namespace User\Register;

class View implements \IView
{
    private $register;
    private $form;

    public function __construct(Model $register, \Form\Model $form)
    {
        $this->register = $register;
        $this->form = $form;
    }

    public function render()
    {
?>
<h2>Inscription</h2>

<form method="POST" class="solid">
    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'general');
    \Form\ViewHelpers::renderErrors($this->form, 'email');
    ?>
    <p>
        <label for="user-register-form-email">Adresse email :</label>
        <input type="email" name="email" required
            id="user-register-form-email"
            value="<?= $this->register->getEmail() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'password');
    ?>
    <p>
        <label for="user-register-form-password">Mot de passe :</label>
        <input type="password" name="password" required
            id="user-register-form-password">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'last-name');
    ?>
    <p>
        <label for="user-register-form-last-name">Nom de famille :</label>
        <input type="text" name="last-name"
            id="user-register-form-last-name"
            value="<?= $this->register->getLastName() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'first-name');
    ?>
    <p>
        <label for="user-register-form-first-name">Prénom :</label>
        <input type="text" name="first-name"
            id="user-register-form-first-name"
            value="<?= $this->register->getFirstName() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'birthdate');
    ?>
    <p>
        <label for="user-register-form-birthdate">Date de naissance :</label>
        <input type="date" name="birthdate"
            id="user-register-form-birthdate"
            value="<?= $this->register->getBirthdate()
                ? $this->register->getBirthdate()->format(\Constants::DATE_ISO_FORMAT)
                : '' ?>"
            max="<?= (new \DateTime())->format(\Constants::DATE_ISO_FORMAT) ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'tel-number');
    ?>
    <p>
        <label for="user-register-form-tel-indic">Numéro de téléphone :</label>
        +<input type="text" name="tel-indic" pattern="[0-9]{1,3}"
            id="user-register-form-tel-indic" size="3" required
            value="<?= $this->register->getTelIndic() ?>">
        <input type="tel" name="tel-number" required
            value="<?= $this->register->getTelNumber() ?>">
    </p>

    <?php
    \Form\ViewHelpers::renderErrors($this->form, 'reside-at');
    ?>
    <p>
        <label for="user-register-form-reside-at">Adresse de résidence :</label>
        <input type="text" name="reside-at"
            id="user-register-form-reside-at"
            value="<?= $this->register->getResideAt() ?>">
    </p>

    <p>
        <input type="submit" value="S’inscrire">
    </p>
</form>
<?php
    }
}
