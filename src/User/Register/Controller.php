<?php
namespace User\Register;

/**
 * Contrôleur du formulare d’inscription utilisateur.
 */
class Controller
{
    private $router;
    private $auth;
    private $register;
    private $form;

    public function __construct(
        \Router $router, \User\Authenticator $auth,
        Model $register, \Form\Model $form
    )
    {
        $this->router = $router;
        $this->auth = $auth;
        $this->register = $register;
        $this->form = $form;
    }

    public function register($data)
    {
        if (empty($data['email']))
        {
            $this->form->addError('email', 'L’adresse email est obligatoire.');
        }
        else
        {
            try
            {
                $this->register->setEmail($data['email']);
            }
            catch (\InvalidArgumentException $err)
            {
                $this->form->addError('email', $err->getMessage());
            }
        }


        if (empty($data['password']))
        {
            $this->form->addError('password', 'Le mot de passe est obligatoire.');
        }
        else
        {
            try
            {
                $this->register->setPassword($data['password']);
            }
            catch (\InvalidArgumentException $err)
            {
                $this->form->addError('password', $err->getMessage());
            }
        }

        try
        {
            $this->register->setLastName($data['last-name']);
        }
        catch (\InvalidArgumentException $err)
        {
            $this->form->addError('last-name', $err->getMessage());
        }

        try
        {
            $this->register->setFirstName($data['first-name']);
        }
        catch (\InvalidArgumentException $err)
        {
            $this->form->addError('first-name', $err->getMessage());
        }

        $birthdate = \DateTime::createFromFormat(
            \Constants::DATE_ISO_FORMAT,
            $data['birthdate']
        );

        if ($birthdate)
        {
            try
            {
                $this->register->setBirthdate($birthdate);
            }
            catch (\InvalidArgumentException $err)
            {
                $this->form->addError('birthdate', $err->getMessage());
            }
        }
        else
        {
            $this->form->addError('birthdate', 'Format de date invalide.');
        }

        try
        {
            $this->register->setTelIndic($data['tel-indic']);
            $this->register->setTelNumber($data['tel-number']);
        }
        catch (\InvalidArgumentException $err)
        {
            $this->form->addError('tel-number', $err->getMessage());
        }

        try
        {
            $this->register->setResideAt($data['reside-at']);
        }
        catch (\InvalidArgumentException $err)
        {
            $this->form->addError('reside-at', $err->getMessage());
        }

        if ($this->form->isFormValid())
        {
            try
            {
                $this->register->persist();
                $this->auth->loginWithoutPassword($this->register->getEmail());
                $this->router->redirect('home');
            }
            catch (\PDOException $err)
            {
                $this->form->addError('general', $err->getMessage());
            }
        }
    }
}

