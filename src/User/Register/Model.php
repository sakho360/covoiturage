<?php
namespace User\Register;

/**
 * Modèle du formulaire d’inscription utilisateur.
 */
class Model
{
    // Référence à la connexion PDO
    private $db;

    // Adresse email du nouvel utilisateur
    private $email;

    // Mot de passe du nouvel utilisateur
    private $password;

    // Nom de famille du nouvel utilisateur
    private $last_name;

    // Prénom du nouvel utilisateur
    private $first_name;

    // Date de naissance du nouvel utilisateur
    private $birthdate;

    // Indicatif téléphonique du numéro de téléphone du nouvel utilisateur
    private $tel_indic = '33';

    // Numbro de téléphone du nouvel utilisateur
    private $tel_number;

    // Adresse de résidence du nouvel utilisateur
    private $reside_at;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function persist()
    {
        $query = <<<SQL
INSERT INTO User(
    email, password, role, last_name, first_name,
    birthdate, tel_indic, tel_number, activation_date,
    reside_at
) VALUES (
    :email, :password, 'user', :last_name, :first_name,
    :birthdate, :tel_indic, :tel_number, NOW(),
    :reside_at
);
SQL;

        $query_vars = [
            ':email' => $this->email,
            ':password' => $this->password,
            ':last_name' => $this->last_name,
            ':first_name' => $this->first_name,
            ':birthdate' => $this->birthdate->format(\Constants::DATE_ISO_FORMAT),
            ':tel_indic' => $this->tel_indic,
            ':tel_number' => $this->tel_number,
            ':reside_at' => $this->reside_at
        ];

        $stmp = $this->db->prepare($query);
        $stmp->execute($query_vars);
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        if (!preg_match('#.*@.*\..*#', $email))
        {
            throw new \InvalidArgumentException(
                'Format de l’adresse email invalide.'
            );
        }

        $this->email = $email;
    }

    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_BCRYPT);
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    public function getBirthdate()
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTime $birthdate)
    {
        if ($birthdate >= new \DateTime())
        {
            throw new \InvalidArgumentException(
                'Félicitations, vous êtes né dans le futur'
            );
        }

        $this->birthdate = $birthdate;
    }

    public function getTelIndic()
    {
        return $this->tel_indic;
    }

    public function setTelIndic($tel_indic)
    {
        if (!preg_match('#[0-9]{1,3}#', $tel_indic))
        {
            throw new \InvalidArgumentException(
                'Format de l’indicatif téléphonique invalide'
            );
        }

        $this->tel_indic = $tel_indic;
    }

    public function getTelNumber()
    {
        return $this->tel_number;
    }

    public function setTelNumber($tel_number)
    {
        if (!preg_match('#[0-9]{1,}#', $tel_number))
        {
            throw new \InvalidArgumentException(
                'Format du numéro de téléphone invalide'
            );
        }

        $this->tel_number = $tel_number;
    }

    public function getResideAt()
    {
        return $this->reside_at;
    }

    public function setResideAt($reside_at)
    {
        $this->reside_at = $reside_at;
    }
}

