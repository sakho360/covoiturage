<?php
namespace User;

/**
 * Énumération des rôles possibles pour les utilisateurs.
 */
class Role
{
    // Simple visiteur du site
    public const NONE = -1;

    // Utilisateur enregistré et authentifié
    public const USER = 0;

    // Administrateur authentifié
    public const ADMIN = 1;
}

