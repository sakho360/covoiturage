<?php
/**
 * Gère le routage des requêtes.
 */
class Router
{
    // Instance d’AltoRouter
    private $router;

    // Instance de la base de données
    private $db;

    // Authentificateur des utilisateurs
    private $auth;

    /**
     * Construit une nouvelle instance du routeur.
     *
     * @param db Instance de la base de données à utiliser.
     * @param auth Authentificateur à utiliser.
     */
    public function __construct(Database $db, User\Authenticator $auth)
    {
        $this->db = $db;
        $this->auth = $auth;

        $this->router = new \AltoRouter();
        $this->router->setBasePath('/covoiturage');

        // Liste des routes de l’application
        $this->router->addRoutes([
            ['GET', '/', 'home', 'home'],

            // Trajet
            ['GET', '/trip/search', 'tripSearch', 'tripSearch'],
            ['GET', '/trip/[i:trip]/start-[i:start]/end-[i:end]', 'tripShow', 'tripShow'],
            ['POST', '/trip/[i:trip]/start-[i:start]/end-[i:end]', 'tripShow'],

            // Utilisateur
            ['GET', '/user/login', 'userLogin', 'userLogin'],
            ['POST', '/user/login', 'userLogin'],
            ['GET', '/user/logout', 'userLogout', 'userLogout'],
            ['GET', '/user/register', 'userRegister', 'userRegister'],
            ['POST', '/user/register', 'userRegister'],
        ]);
    }

    /**
     * Analyse le chemin actuel pour déterminer la route correspondante,
     * et affiche la page en conséquence. Si le chemin ne correspond à
     * aucune route, affiche une page d’erreur.
     */
    public function route()
    {
        $match = $this->router->match();

        if ($match)
        {
            call_user_func_array(
                array($this, $match['target']),
                $match['params']
            );
        }
        else
        {
            http_response_code(404);
            die('Page non trouvée.');
        }
    }

    /**
     * Redirige l’utilisateur vers une route.
     *
     * @param route Nom de la route vers laquelle rediriger.
     * @param [params=[]] Paramètres à appliquer à la route.
     * @return (Ne retourne pas.)
     */
    public function redirect($route, array $params = [])
    {
        header('Location: ' . $this->router->generate($route, $params));
        exit;
    }

    /**
     * Génère une URL vers une route paramétrée.
     *
     * @param route Nom de la route à générer.
     * @param [params=[]] Paramètres à appliquer à la route.
     * @return URL générée.
     */
    public function generate($route, array $params = [])
    {
        return $this->router->generate($route, $params);
    }

    /**
     * Page d’accueil.
     */
    private function home()
    {
        $model = new Site\Home\Model($this->db->get(), $this->auth);
        $home = new Site\Home\View($this, $this->auth, $model);

        $template_model = new Site\Template\Model();
        $template_model->setTitle('Accueil');

        $template = new Site\Template\View($this, $this->auth, $home, $template_model);
        $template->render();
    }

    /**
     * Recherche de trajet.
     */
    private function tripSearch()
    {
        $search = new Trip\Search\SearchModel($this->db->get());
        $form = new Form\Model();

        $controller = new Trip\Search\FormController($search, $form);

        $form = new Trip\Search\FormView($search, $form);
        $results = new Trip\Search\ResultsView($this, $search);
        $content = new Trip\Search\SearchView($form, $results);

        if (!empty($_GET))
        {
            $controller->search($_GET);
        }

        $template_model = new Site\Template\Model();
        $template_model->setTitle('Rechercher un trajet');

        $template = new Site\Template\View($this, $this->auth, $content, $template_model);
        $template->render();
    }

    /**
     * Détails d’un trajet.
     *
     * @param trip Identifiant du trajet.
     * @param start Identifiant du départ du trajet.
     * @param end Identifiant de l’arrivée du trajet.
     */
    private function tripShow($trip, $start, $end)
    {
        $model = new Trip\Detail\Model($this->db->get(), $this->auth);
        $form = new Form\Model();

        $view = new Trip\Detail\View($this, $this->auth, $model, $form);
        $controller = new Trip\Detail\Controller($model, $form);

        $template_model = new Site\Template\Model();
        $template_model->setTitle('Détails du trajet');

        $action = 'show';

        if (!empty($_POST['action']) &&
                ($_POST['action'] === 'add' || $_POST['action'] === 'remove'))
        {
            $action = $_POST['action'];
        }

        $controller->{$action}($trip, $start, $end);

        $template = new Site\Template\View($this, $this->auth, $view, $template_model);
        $template->render();
    }

    /**
     * Connexion d’un utilisateur.
     */
    private function userLogin()
    {
        $model = new User\Login\Model();
        $form = new Form\Model();

        $controller = new User\Login\Controller($this, $this->auth, $model, $form);

        $content = new User\Login\View($model, $form);

        if (!empty($_POST))
        {
            $controller->login($_POST);
        }

        $template_model = new Site\Template\Model();
        $template_model->setTitle('Connexion');

        $template = new Site\Template\View($this, $this->auth, $content, $template_model);
        $template->render();
    }

    /**
     * Déconnexion de l’utilisateur courant.
     */
    private function userLogout()
    {
        $this->auth->logout();
        $this->redirect('home');
    }

    /**
     * Inscription d’un nouvel utilisateur.
     */
    private function userRegister()
    {
        $model = new User\Register\Model($this->db->get());
        $form = new Form\Model();

        $controller = new User\Register\Controller(
            $this, $this->auth,
            $model, $form
        );

        $content = new User\Register\View($model, $form);

        if (!empty($_POST))
        {
            $controller->register($_POST);
        }

        $template_model = new Site\Template\Model();
        $template_model->setTitle('Inscription');

        $template = new Site\Template\View(
            $this, $this->auth,
            $content, $template_model
        );
        $template->render();
    }
}
