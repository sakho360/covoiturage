<?php
namespace Site\Template;

/**
 * Squelette global du site.
 */
class View implements \IView
{
    private $router;
    private $auth;
    private $content;
    private $model;

    /**
     * Construit une nouvelle instance du squelette.
     *
     * @param router Instance du routeur.
     * @param auth Instance de l’authentifieur.
     * @param content Vue à afficher dans le contenu de la page.
     * @param model Modèle du squelette.
     */
    public function __construct(
        \Router $router, \User\Authenticator $auth,
        \IView $content, Model $model
    )
    {
        $this->router = $router;
        $this->auth = $auth;
        $this->content = $content;
        $this->model = $model;
    }

    public function render()
    {
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Covoiturage – <?= $this->model->getTitle() ?></title>
        <link rel="stylesheet" href="/covoiturage/static/style/main.css">
    </head>
    <body>
        <header>
            <h1>Covoiturage</h1>
        </header>

        <nav>
            <ul class="navigation">
                <li><a href="<?= $this->router->generate('home') ?>">Accueil</a></li>
                <li><a href="<?= $this->router->generate('tripSearch') ?>">Rechercher un trajet</a></li>
            </ul>

            <ul class="connection">
                <?php
                if ($this->auth->hasRole(\User\Role::USER)):
                ?>
                <li><a href="<?= $this->router->generate('userLogout') ?>">Déconnectez-vous</a></li>
                <?php
                else:
                    ?>
                <li><a href="<?= $this->router->generate('userLogin') ?>">Se connecter</a></li>
                <li><a href="<?= $this->router->generate('userRegister') ?>">S’inscrire</a></li>
                <?php
                endif;
                ?>
            </ul>
        </nav>

        <?php $this->content->render(); ?>
    </body>
</html>
<?php
    }
}
