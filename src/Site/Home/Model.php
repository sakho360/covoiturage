<?php
namespace Site\Home;

/**
 * Modèle de la page d’accueil.
 */
class Model
{
    // Instance de la base de données
    private $db;

    // Instance de l’authentificateur
    private $auth;

    public function __construct(\PDO $db, \User\Authenticator $auth)
    {
        $this->db = $db;
        $this->auth = $auth;
    }

    /**
     * Récupère les informations de l’utilisateur actuellement
     * connecté.
     *
     * @return stdClass Objet contenant les informations sur l’utilisateur
     * actuellement connecté ou NULL s’il n’y en a pas.
     */
    public function getUser()
    {
        if (!$this->auth->hasRole(\User\Role::USER))
        {
            return NULL;
        }

        $query = <<<SQL
SELECT full_name, role
  FROM UserDisplay
 WHERE email = :email;
SQL;

        $stmp = $this->db->prepare($query);
        $stmp->execute(['email' => $this->auth->getEmail()]);

        return $stmp->fetch(\PDO::FETCH_OBJ);
    }
}

