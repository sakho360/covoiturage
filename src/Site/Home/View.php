<?php
namespace Site\Home;

/**
 * Page d’accueil du site.
 */
class View implements \IView
{
    // Instance du routeur
    private $router;

    // Instance de l’authentificateur
    private $auth;

    // Modèle de la vue
    private $model;

    /**
     * Construit une instance de la page d’accueil.
     *
     * @param router Instance du routeur.
     * @param auth Authentificateur d’utilisateur.
     * @param model Modèle de la vue.
     */
    public function __construct(
        \Router $router, \User\Authenticator $auth,
        Model $model
    )
    {
        $this->router = $router;
        $this->auth = $auth;
        $this->model = $model;
    }

    public function render()
    {
        $infos = $this->model->getUser();

        if ($infos === NULL):
?>
<div class="solid">
    <p>
        Bienvenue sur le site Covoiturage.
    </p>
    <p>
        Vous n’êtes actuellement pas connecté. Pour accéder à l’ensemble
        des contenus, veuillez
        <a href="<?= $this->router->generate('userLogin') ?>">vous identifier.</a>
    </p>
</div>
<?php
        else:
?>
<p>
    Bonjour <?= $infos->full_name ?>
    (<?= $infos->role === 'user' ? 'utilisateur' : 'administrateur' ?>).
</p>
<?php
        endif;
    }
}
