<?php
/**
 * Gère la connexion à la base de données.
 */
class Database
{
    private $pdo;

    /**
     * Construit une nouvelle connexion à la base de données.
     *
     * @param database Base à utiliser.
     * @param user Utilisateur autorisé à accéder à la base.
     * @param password Mot de passe de l’utilisateur.
     * @param [host=localhost] Serveur de la base de données.
     */
    public function __construct($database, $user, $password, $host = 'localhost')
    {
        $this->pdo = new PDO(
            'mysql:host=' . $host . ';dbname=' . $database . ';charset=UTF8',
            $user, $password, [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false
            ]
        );
    }

    /**
     * Récupère l’instance PDO de la base de données.
     *
     * @return Instance PDO connectée à la base.
     */
    public function get()
    {
        return $this->pdo;
    }
}

