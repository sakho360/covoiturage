<?php
/**
 * Vue pouvant être affichée à l’utilisateur.
 */
interface IView
{
    /**
     * Affiche les données de la vue sur la sortie standard.
     */
    public function render();
}
