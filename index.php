<?php
// Ouverture de la session
session_start();

// Configuration de la langue
setlocale(LC_ALL, 'fr_FR.utf8');

// Configuration du chargeur de classes
spl_autoload_register(function ($class)
{
    require __DIR__ . '/src/'
        . str_replace('\\', DIRECTORY_SEPARATOR, $class)
        . '.php';
});

// Chargement des bibliothèques installées via Composer
require __DIR__ . '/vendor/autoload.php';

// Connexion à la base de données
try
{
    $db = new \Database(
        'covoiturage', 'covoiturage',
        'covoiturage'
    );
}
catch (\PDOException $err)
{
    die(
        "Impossible d’établir une connexion à la base de données :\n"
        . $err->getMessage()
    );
}

// Authentification de l’utilisateur
$auth = new User\Authenticator($db->get());

// Routage de la requête
$router = new Router($db, $auth);
$router->route();

